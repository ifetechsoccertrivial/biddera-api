from lib.elastic import Elastic
from datetime import datetime
import lib.rabbit
import json
from config.config import CONFIG
from lib.elastic import Elastic
import lib.rabbit
import json
from config.config import CONFIG


class BidModel:
    def __init__(self):

        self.db = Elastic()

    def update(self, doc_id, data):
        result = {
            "updated_at": datetime.now(),
            "bid": data.bid
        }
        self.db.update(index='bid-index', doc_type='bid', doc_id=doc_id, body=result)

    def get(self, doc_id):
        data = self.db.query(index='bid-index', doc_type='bid', doc_id=doc_id)
        return data

    def save(self, data):
        param = {
            "user_id": data['user_id'],
            "product_id": data['product_id'],
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
            "bid": data['bid'],
        }
        self.db.mapping()
        result = self.db.insert('bid-index', 'bid', body=param)
        print result

    def search(self):
        dsl = {
            "query": {
                "match_all": {}
            }
        }
        data = self.db.search(index='bid-index', body=dsl)
        return data

    def query(self, data):
        dsl = {
            "query": {
                "bool": {
                      "must": [
                        {
                          "match": {"user_id": data['user_id'],}
                        },
                        {
                          "match": {"product_id": data['product_id']}
                        }
                      ]

                }
            }
        }
        result = self.db.search(index='bid-index', body=dsl)
        return result

