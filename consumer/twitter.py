import lib.rabbit
from config.config import CONFIG


class TwitterConsumer:
    def __init__(self):
        queue = (CONFIG.get('queue')).get('twitter')
        self._rabbit = lib.rabbit.RabbitMQ(queue)

    def consume(self):
        print "data in consumer"
        self._rabbit.consumer()

