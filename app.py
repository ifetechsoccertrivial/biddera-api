import sys
from job import TwitterJob
from consumer import TwitterConsumer
from colorama import init
from colorama import Fore, Back, Style

init()


def main():
    args = sys.argv
    if not len(args) <= 1:
        if "job":
            print "Initializing Listen for 1 hour"
            TwitterJob().launch_stream(args[2])
        else:
            print "Initializing Consumer"
            TwitterConsumer().consume()
    else:
        print args
        print Fore.RED + "Provide argument job/consumer to initialize either"
        print Style.RESET_ALL

if __name__ == "__main__":
    main()
