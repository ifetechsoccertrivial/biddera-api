import web
import json
from job.facebook import Facebook
from model import UserTwitterModel

urls = (
    '/user', 'User'
)


class User:

    def __init__(self):
        pass

    def create(self, result):
        #print result
        data = json.loads(result)
        user = UserTwitterModel()
        user.twitter_id = data["id"]
        user.name = data["user"]["name"]
        user.screen_name = data["user"]["screen_name"]
        user.profile_image = data["user"]["profile_image_url"]
        user.campaign = data["entities"]["hashtags"]
        user.save()


if __name__ == "__main__":
    app = web.application(urls, globals())
    web.httpserver.runsimple(app.wsgifunc(), ("127.0.0.1", 5000))
    app.run()
