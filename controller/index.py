import web
from lib.response import Response
from model.bid import BidModel


urls = (
    '/index', 'Index'
)


class Index:

    def __init__(self):
        self._model = BidModel()

    def GET(self):
        data = self._model.search()
        return Response.response_ok(data=data)

    def POST(self):
        user_data = web.input()
        error = self.validate(user_data)
        if len(error) > 0:
            return Response.response_error("Invalid parameters", error)
            
        myData = {
            'user_id': user_data.user_id,
            'product_id': user_data.product_id,
            'bid': user_data.bid
        }

        result = self._model.query(myData)
        total = result['hits']['total']

        if total > 0:
            return self.update(result, myData)
        else:
            return self.create(user_data=myData)

    def create(self, user_data):
        myData = {
            'user_id': user_data['user_id'],
            'product_id': user_data['product_id'],
            'bid': int(user_data['bid'])
        }
        self._model.save(data=myData)
        return Response.response_ok(data=myData)

    def update(self, result, update):
        doc_id = result['_id']
        data = self._model.update(doc_id, update)
        return Response.response_ok(data=data)

    def validate(self, user_data):
        error = {}
        if hasattr(user_data, 'user_id'):
            user_id = user_data.user_id
            if len(user_id) <= 2:
                error['user_id'] = "Please give a valid user id"
        else:
            error['user_id'] = "User id field is missing"

        if hasattr(user_data, 'product_id'):
            product_id = user_data.product_id
            if len(product_id) <= 2:
                error['192.168.1.211product_id'] = "Missing product id"
        else:
            error['product_id'] = "Product id field is missing"

        if hasattr(user_data, 'bid'):
            bid = user_data.bid
            if len(bid) <= 2:
                error['bid'] = "Missing bid"
        else:
            error['bid'] = "bid field is missing"

        return error


if __name__ == "__main__":
    app = web.application(urls, globals())
    web.httpserver.runsimple(app.wsgifunc(), ("127.0.0.1", 5000))
    app.run()