from tweepy import StreamListener
from config.config import CONFIG
from lib.rabbit import RabbitMQ


class Listener(StreamListener):
    def on_status(self, status):
        print status.text

    def on_data(self, raw_data):
        queue_name = (CONFIG.get('queue')).get('twitter')
        rabbit = RabbitMQ(queue_name)
        rabbit.producer(raw_data)
        print "Received Data"