import facebook
from config.config import CONFIG


class Facebook:
    def __init__(self):
        access_token = (CONFIG.get('facebook')).get('facebook_key')
        # init facebook sdk with short live token
        self._facebook_graph = facebook.GraphAPI(access_token=access_token, version='2.6')

        long_access_token = self.page_access_token()

        self._facebook_graph = facebook.GraphAPI(access_token=long_access_token['access_token'])

        print long_access_token
        # print access_token

    def page_access_token(self):
        app_id = (CONFIG.get('facebook')).get('app_id')
        access_secret = (CONFIG.get('facebook')).get('access_secret')

        long_live_access_token = self._facebook_graph.extend_access_token(app_id=app_id, app_secret=access_secret)
        return long_live_access_token

    def get_object(self, page):
        post = self._facebook_graph.get_object(page)
        print post

if __name__ == "__main__":
    _facebook = Facebook()
    _facebook.get_object("/1683308141884717/feed")
