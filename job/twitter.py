from tweepy import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from config.config import CONFIG
from lib.rabbit import RabbitMQ
from listener import Listener


class TwitterJob:
    def __init__(self):
        twitter_config = CONFIG.get('twitter')
        listener = Listener()
        auth = OAuthHandler(twitter_config.get('consumer_api_key'), twitter_config.get('consumer_api_secret'))
        auth.set_access_token(twitter_config.get('access_token'), twitter_config.get('access_token_secret'))
        self.stream = Stream(auth, listener)

    def launch_stream(self, param):
        self.stream.filter(track=param)

    def exit_stream(self):
        self.stream.timeout()
