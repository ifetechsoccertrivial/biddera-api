import json
import web


class Response:
    def __init__(self):
        self._response_ok = []
        self._response_error = []

    @staticmethod
    def response_ok(data):
        web.header('Content-Type', 'application/json')
        response = {'status': 'success', 'data': data}
        return json.dumps(response)

    @staticmethod
    def response_error(message, error):
        web.header('Content-Type', 'application/json')
        response = {'status': 'fail', 'message': message, 'error': error}
        return json.dumps(response)
